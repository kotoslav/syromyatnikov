import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Гетбайид-тестовый', description: '', gender: 'male' }];

let catId: Number;

const notValidCatId: String = "гетбайид";
const testDecription: String = "А зачем тебе это описание...";


const HttpClient = Client.getInstance();

describe('cats-api-core tests', () => {

  beforeAll(async () => {
    try {
      const add_cat_response = await HttpClient.post('cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не получилось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    await HttpClient.delete(`cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

  it('Поиск существующего котика по id', async () => {
    const response = await HttpClient.get(`cats/get-by-id?id=${catId}`, {
      responseType: 'json',
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toMatchObject({
      cat: {
        id: catId,
        name: cats[0].name
      }
    });
  });


  it('Поиск некорректного id котика', async () => {
    await expect (
      HttpClient.get(`cats/get-by-id?id=${notValidCatId}`, {
      responseType: 'json',
    })).rejects.toThrowError('Response code 400 (Bad Request)');
  });

  it('Проверка добавления описания котику', async () => {
    await HttpClient.post(`cats/save-description`, {
      responseType: 'json',
      json: {
        catId,
        catDescription: testDecription
      }
    });

    const response = await HttpClient.get(`cats/get-by-id?id=${catId}`, {
      responseType: 'json',
    });

    expect(response.statusCode).toEqual(200);

    expect(response.body).toMatchObject({
      cat: {
        id: catId,
        description: testDecription,
      }
    });
  })

  it('Получение списка котов сгруппированных по группам', async () => {
    const response = await HttpClient.get('cats/allByLetter', {
      responseType: 'json',
    });
    expect(response.statusCode).toEqual(200);

  expect(response.body).toEqual({
    groups: expect.arrayContaining([
      expect.objectContaining({
        title: expect.stringMatching(/[А-Я]/),
        count_in_group: expect.any(Number),
        count_by_letter: expect.any(Number),
        cats: expect.arrayContaining([
          expect.objectContaining({
            count_by_letter: expect.any(String),
            id: expect.any(Number),
            name: expect.any(String),
            description: expect.any(String),
            tags: expect.any(Object),
            gender: expect.stringMatching(/male|female|unisex/),
            likes: expect.any(Number),
            dislikes: expect.any(Number)
          })
        ]
        )
      }
    )]),
    count_all: expect.any(Number),
    count_output: expect.any(Number)
  });

  });

});
